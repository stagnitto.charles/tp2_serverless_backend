resource aws_iam_role "iam_for_s3_to_sqs_lambda"{
  name = "iam_for_s3_to_sqs_lambda"
  assume_role_policy = file("files/lambda_assume_role_policy.json")
}

resource aws_iam_role_policy "lambda_logging_for_s3_to_sqs"{
    name   = "lambda_logging_for_s3_to_sqs"
    role   = aws_iam_role.iam_for_s3_to_sqs_lambda.id
    policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": "${aws_s3_bucket.s3_job_offer_bucket_duchesne_bertin_stagnitto.arn}/*"
        }
    ]
})
}

resource aws_iam_role_policy "iam_policy_for_sqs_sender"{
    name   = "iam_policy_for_sqs_sender"
    role   = aws_iam_role.iam_for_s3_to_sqs_lambda.id
    policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": "${aws_s3_bucket.s3_job_offer_bucket_duchesne_bertin_stagnitto.arn}/*"
        }
    ]
})
}

resource aws_iam_role_policy "iam_policy_for_s3"{
    name   = "iam_policy_for_s3"
    role   = aws_iam_role.iam_for_s3_to_sqs_lambda.id
    policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "sqs:SendMessage",
            "Resource": "${aws_sqs_queue.job_offers_queue.arn}"
        }
    ]
    })
}

resource aws_iam_role "iam_for_sqs_to_dynamo_lambda"{
    name = "iam_for_sqs_to_dynamo_lambda"
    assume_role_policy  = jsonencode({
  Version: "2012-10-17",
  Statement: [

    {
      Action: "sts:AssumeRole",
      Principal: {
        Service: "lambda.amazonaws.com"
      },
      Effect: "Allow"
    }
  ]
})
}

resource "aws_iam_role_policy" "lambda_logging_for_sqs_to_dynamo" {
  name   = "sqs_to_dynamo_lambda_policy"
  role   = aws_iam_role.iam_for_sqs_to_dynamo_lambda.id

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = [
          "sqs:ReceiveMessage",
          "sqs:DeleteMessage",
          "sqs:GetQueueAttributes"
        ],
        Resource = aws_sqs_queue.job_offers_queue.arn
      },
      {
        Effect = "Allow",
        Action = "dynamodb:PutItem",
        Resource = aws_dynamodb_table.job-table.arn
      }
    ]
  })
}

resource aws_iam_role_policy "iam_policy_for_dynamodb" {
    name   = "iam_policy_for_dynamodb"
    role   = aws_iam_role.iam_for_sqs_to_dynamo_lambda.id
    policy = jsonencode({
    Version: "2012-10-17",
    Statement: [
        {
            Effect: "Allow",
            Action: "dynamodb:PutItem",
            Resource: aws_dynamodb_table.job-table.arn
        }
    ]
    })
}

resource aws_iam_role "job_api_lambda_role"{
  name = "job_api_lambda_role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "lambda.amazonaws.com",
        },
      },
    ],
  })
}

resource "aws_iam_role_policy" "job_api_lambda_policy" {
  name   = "dynamodb_full_access_policy"
  role   = aws_iam_role.job_api_lambda_role.name

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect   = "Allow",
        Action   = "dynamodb:*",
        Resource = aws_dynamodb_table.job-table.arn,
      },
    ],
  })
}

resource "aws_iam_role_policy_attachment" "lambda_exec_policy_attachment" {
  policy_arn = var.lambda_exec_policy_arn
  role       = aws_iam_role.job_api_lambda_role.name
}

