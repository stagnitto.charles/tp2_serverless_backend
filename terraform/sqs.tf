resource aws_sqs_queue "job_offers_queue"{
  name                      = "job-offers-queue"
  delay_seconds             = 90
  max_message_size          = 2048
  message_retention_seconds = 86400
  receive_wait_time_seconds = 10
  visibility_timeout_seconds = 900
  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.job_offers_queue_dead_letter.arn
    maxReceiveCount     = 4
  })

  tags = {
    Environment = "production"
  }
}

resource aws_sqs_queue "job_offers_queue_dead_letter" {
    name                      = "job-offers-queue-deadletter"
    delay_seconds             = 90
    max_message_size          = 2048
    message_retention_seconds = 86400
    receive_wait_time_seconds = 10

    tags = {
        Environment = "production"
    }
}

/* Finally, we will need an aws_lambda_event_source_mapping
with the job_offers_queue ARN as event_source_arn and the sqs_to_dynamo_lambda's name as function_name */
resource aws_lambda_event_source_mapping "sqs_to_dynamo" {
  event_source_arn = aws_sqs_queue.job_offers_queue.arn
  function_name    = aws_lambda_function.sqs_to_dynamo_lambda.arn
}
