data "archive_file" "empty_zip_code_lambda" {
  type        = "zip"
  output_path = "empty_lambda_code.zip"
  source {
    content  = "hello_world"
    filename = "dummy.txt"
  }
}

resource aws_lambda_function "s3_to_sqs_lambda"{
  role = aws_iam_role.iam_for_s3_to_sqs_lambda.arn
  function_name = "s3_to_sqs_lambda"
  handler = "index.handler"
  memory_size = 512
  timeout = 900
  runtime = "nodejs18.x"
    filename = data.archive_file.empty_zip_code_lambda.output_path
  environment {
    variables = {
      QUEUE_URL = aws_sqs_queue.job_offers_queue.url
    }
  }
}

resource aws_lambda_permission "allow_bucket"{
  statement_id = "AllowExecutionFromS3Bucket"
  action = "lambda:InvokeFunction"
    function_name = aws_lambda_function.s3_to_sqs_lambda.function_name
    principal = "s3.amazonaws.com"
}


resource aws_lambda_function "sqs_to_dynamo_lambda"{
role = aws_iam_role.iam_for_sqs_to_dynamo_lambda.arn
  function_name = "sqs_to_dynamo_lambda"
  handler = "index.handler"
  memory_size = 512
  timeout = 900
  runtime = "nodejs18.x"
    filename = data.archive_file.empty_zip_code_lambda.output_path
  environment {
    variables = {
      TABLE_NAME = aws_dynamodb_table.job-table.name
    }
  }
}

resource aws_lambda_permission "allow_sqs_queue"{
    statement_id = "AllowExecutionFromSQSQueue"
    action = "lambda:InvokeFunction"
        function_name = aws_lambda_function.sqs_to_dynamo_lambda.function_name
        principal = "sqs.amazonaws.com"
}

resource aws_lambda_function "job_api_lambda"{
  handler = "lambda.handler"
  role = aws_iam_role.job_api_lambda_role.arn
    function_name = "job_api_lambda"
  memory_size = 512
  timeout = 30
  runtime = "nodejs18.x"
    filename = data.archive_file.empty_zip_code_lambda.output_path
  environment {
    variables = {
      TABLE_NAME = aws_dynamodb_table.job-table.name
    }
  }
}
